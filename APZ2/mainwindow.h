#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_KSVM_clicked();

    void on_KvoMet_clicked();

    void on_KvoKlass_clicked();

    void on_KSVK_clicked();

    void on_KvoModul_clicked();

    void on_KUModul_clicked();

    void on_WMC_clicked();

    void on_RFC_clicked();

    void on_NOC_clicked();

    void on_Exit_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
